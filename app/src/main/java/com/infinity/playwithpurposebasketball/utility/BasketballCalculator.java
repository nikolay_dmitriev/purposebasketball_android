package com.infinity.playwithpurposebasketball.utility;

import com.infinity.playwithpurposebasketball.database.dataObjects.Game;

public class BasketballCalculator {

    public static String getRewardName(Game game) {
        int assist = tryToGetPoint(game.getStatistic().getAssist());
        int block = tryToGetPoint(game.getStatistic().getBlock());
        int steal = tryToGetPoint(game.getStatistic().getSteal());

        int oreb = game.getStatistic().getOreb();
        int dreb = game.getStatistic().getDreb();
        int rebounds = tryToGetPoint(oreb + dreb);

        int points = tryToGetPoint(getTotalPoints(game));

        int result = assist + block + steal + rebounds + points;

        return parseRewardName(result);
    }

    public static int getTotalPoints(Game game) {
        return game.getStatistic().getTwoPointsMake() * 2 +
                game.getStatistic().getThreePointsMake() * 3 +
                game.getStatistic().getFreeThrowMake();
    }

    public static int getPwpPoints(Game game){
        int points = getTotalPoints(game);
        points += game.getStatistic().getAssist() * 2;
        points += game.getStatistic().getSteal() * 2;
        points += game.getStatistic().getBlock() * 2;
        points += game.getStatistic().getDreb() * 2;
        points += game.getStatistic().getOreb() * 2;
        return points;
    }

    public static int calculatePercentage(int number1, int number2) {
        if (number1 == 0) {
            return 0;
        }
        if (number2 == 0) {
            return 100;
        }
        float result = (float) number1 / (number1 + number2);
        return (int) (result * 100);
    }

    private static String parseRewardName(int value) {
        switch (value) {
            case 2:
                return "Double-Double";
            case 3:
                return "Triple-Double";
            case 4:
                return "Quad-Double";
            case 5:
                return "Quintuple-Double";
            default:
                return "";
        }
    }

    private static int tryToGetPoint(int value) {
        return value >= 10 ? 1 : 0;
    }
}
