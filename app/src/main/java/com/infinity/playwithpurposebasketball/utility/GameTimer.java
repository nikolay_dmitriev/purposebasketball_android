package com.infinity.playwithpurposebasketball.utility;

import android.graphics.Color;
import android.widget.TextView;

import com.infinity.playwithpurposebasketball.MainApplication;
import com.infinity.playwithpurposebasketball.activities.GameActivity;
import com.infinity.playwithpurposebasketball.database.dataObjects.Game;

import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class GameTimer {
    private WeakReference<TextView> weakTextViewToUpdate;
    private WeakReference<GameActivity> weakActivity;
    private Game game;

    private Timer timer;

    public final static int maxTime = 30 * 60; //Max time is 30 minutes

    private int currentTime = 0;

    public GameTimer(Game game, TextView textView, GameActivity activity) {
        weakActivity = new WeakReference<>(activity);
        weakTextViewToUpdate = new WeakReference<>(textView);
        this.game = game;
        currentTime = game.getGameLengthInSecond();
        updateUi();
    }

    public void start() {
        weakTextViewToUpdate.get().setTextColor(Color.GREEN);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                weakActivity.get().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onTimerTick();
                    }
                });
            }
        }, 1000, 1000);
    }

    public void stop() {
        if (timer != null) {
            weakTextViewToUpdate.get().setTextColor(Color.WHITE);
            timer.cancel();
            timer = null;
        }
    }

    public boolean isTimerActive() {
        return timer != null;
    }

    private void onTimerTick() {
        currentTime++;

        currentTime = Math.min(currentTime, maxTime);

        MainApplication.getInstance().getRealm().beginTransaction();
        game.setGameLengthInSecond(currentTime);
        MainApplication.getInstance().getRealm().commitTransaction();

        updateUi();
    }

    private void updateUi() {
        int minutes = currentTime / 60;
        int seconds = currentTime - minutes * 60;
        String text = String.format(Locale.US, "Time: %02d:%02d", minutes, seconds);
        weakTextViewToUpdate.get().setText(text);
    }

    public void setTime(int seconds){
        currentTime = seconds;
        MainApplication.getInstance().getRealm().beginTransaction();
        game.setGameLengthInSecond(currentTime);
        MainApplication.getInstance().getRealm().commitTransaction();

        updateUi();
    }

    public int getTime(){
        return game.getGameLengthInSecond();
    }

}
