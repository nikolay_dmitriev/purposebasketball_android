package com.infinity.playwithpurposebasketball.recycleView;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.infinity.playwithpurposebasketball.MainApplication;
import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.activities.CreateGameActivity;
import com.infinity.playwithpurposebasketball.activities.GameActivity;
import com.infinity.playwithpurposebasketball.database.dataObjects.Game;
import com.infinity.playwithpurposebasketball.utility.BasketballCalculator;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class GamesListAdapter extends RecyclerView.Adapter {

//region Variables

    private List<Game> items;
    private WeakReference<Context> weakContext;

//endregion

//region Public

    public GamesListAdapter(Context context) {
        weakContext = new WeakReference<>(context);
        items = new ArrayList<>();
        updateData();
    }

    public void remove(int position) {
        Game item = items.get(position);
        if (items.contains(item)) {
            MainApplication.getInstance().getRealm().beginTransaction();
            item.deleteFromRealm();
            MainApplication.getInstance().getRealm().commitTransaction();

            items.remove(position);

            notifyItemRemoved(position);
        }
    }

//endregion

//region RecycleViewAdapter

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GameViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_games_list_recycle_view_row, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        GameViewHolder viewHolder = (GameViewHolder) holder;
        final Game item = items.get(position);

        String result = item.getName();
        String gameNumber = item.getPlayerNumber();
        String tournament = item.getTournamentName();

        if(!gameNumber.isEmpty()){
            result += ", #" + gameNumber;
        }
        if(!tournament.isEmpty()){
            result += ", " + tournament;
        }

        viewHolder.titleTextView.setText(result);

        ImageView avatarImageView = viewHolder.avatarImageView;

        Glide.with(weakContext.get()).load(item.getAvatarPath()).placeholder(R.drawable.place_basketball).into(avatarImageView);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        viewHolder.dateTextView.setText(dateFormat.format(item.getDate()));

        viewHolder.pointsTextView.setText(String.valueOf(BasketballCalculator.getTotalPoints(item)));
        viewHolder.assistTextView.setText(String.valueOf(item.getStatistic().getAssist()));
        viewHolder.stealTextView.setText(String.valueOf(item.getStatistic().getSteal()));
        viewHolder.blockTextView.setText(String.valueOf(item.getStatistic().getBlock()));
        viewHolder.orebDrebTextView.setText(String.valueOf(item.getStatistic().getOreb() + item.getStatistic().getDreb()));

        viewHolder.pwpPointsTextView.setText(String.format(Locale.US, "PWP Points: %d", BasketballCalculator.getPwpPoints(item)));

        String rewardName = BasketballCalculator.getRewardName(item);
        if(rewardName.isEmpty()){
            viewHolder.rewardImageView.setVisibility(View.GONE);
        }else {
            viewHolder.rewardImageView.setVisibility(View.VISIBLE);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailView = new Intent(weakContext.get(), GameActivity.class);
                detailView.putExtra(GameActivity.gameIdKey, item.getId());
                weakContext.get().startActivity(detailView);
            }
        });

        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent editActivityIntent = new Intent(weakContext.get(), CreateGameActivity.class);
                editActivityIntent.putExtra(CreateGameActivity.gameIdKey, item.getId());
                weakContext.get().startActivity(editActivityIntent);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateData() {
        items.clear();

        Realm realm = MainApplication.getInstance().getRealm();
        RealmQuery<Game> query = realm.where(Game.class);
        RealmResults<Game> queryResult = query.findAll().sort("date", Sort.DESCENDING);

        for (Game game : queryResult) {
            items.add(game);
        }

        notifyDataSetChanged();
    }

//endregion


}
