package com.infinity.playwithpurposebasketball.recycleView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.infinity.playwithpurposebasketball.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameViewHolder extends RecyclerView.ViewHolder{
    @BindView(R.id.avatarImageView)
    public ImageView avatarImageView;

    @BindView(R.id.titleTextView)
    public TextView titleTextView;

    @BindView(R.id.dateTextView)
    public TextView dateTextView;

    @BindView(R.id.pointsTextView)
    public TextView pointsTextView;

    @BindView(R.id.assistTextView)
    public TextView assistTextView;

    @BindView(R.id.stealTextView)
    public TextView stealTextView;

    @BindView(R.id.blockTextView)
    public TextView blockTextView;

    @BindView(R.id.orebdrebTextView)
    public TextView orebDrebTextView;

    @BindView(R.id.pwpPointsTextView)
    public TextView pwpPointsTextView;

    @BindView(R.id.rewardBallImageView)
    ImageView rewardImageView;

    public GameViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
