package com.infinity.playwithpurposebasketball.views;

import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

public class StatisticGridButton {

//region Variables

    private boolean isEnabled = false;
    private WeakReference<StatisticGrid> weakParent;

//endregion

//region Public

    public StatisticGridButton(ViewGroup viewGroup, StatisticGrid parent, final ButtonLogic logic) {
        weakParent = new WeakReference<>(parent);

        viewGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logic.onClick();
                weakParent.get().updateUi();
            }

        });
        
        viewGroup.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                logic.onLongClick();
                weakParent.get().updateUi();
                return true;
            }
        });

    }

    public void enable() {
        isEnabled = true;
    }

    public void disable() {
        isEnabled = false;
    }

//endregion


    public interface ButtonLogic {
        void onClick();

        void onLongClick();
    }

}

