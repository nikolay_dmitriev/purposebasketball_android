package com.infinity.playwithpurposebasketball.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.infinity.playwithpurposebasketball.MainApplication;
import com.infinity.playwithpurposebasketball.MainConstants;
import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.activities.GameActivity;
import com.infinity.playwithpurposebasketball.database.dataObjects.Game;
import com.infinity.playwithpurposebasketball.database.dataObjects.GameStatistic;
import com.infinity.playwithpurposebasketball.utility.BasketballCalculator;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class StatisticGrid extends LinearLayout {

//region Two Points Row

    @BindView(R.id.twoPointsMakeLayout)
    ViewGroup twoPointsMakeLayout;

    @BindView(R.id.twoPointsMakeTextView)
    TextView twoPointsMakeTextView;

    @BindView(R.id.twoPointsMissLayout)
    ViewGroup twoPointsMissLayout;

    @BindView(R.id.twoPointsMissTextView)
    TextView twoPointsMissTextView;

    @BindView(R.id.twoPointsPtsTextView)
    TextView twoPointsPtsTextView;

    @BindView(R.id.twoPointsPercentageTextView)
    TextView twoPointsPercentageTextView;

//endregion

//region ThreePoints

    @BindView(R.id.threePointsMakeLayout)
    ViewGroup threePointsMakeLayout;

    @BindView(R.id.threePointsMakeTextView)
    TextView threePointsMakeTextView;

    @BindView(R.id.threePointsMissLayout)
    ViewGroup threePointsMissLayout;

    @BindView(R.id.threePointsMissTextView)
    TextView threePointsMissTextView;

    @BindView(R.id.threePointsPtsTextView)
    TextView threePointsPtsTextView;

    @BindView(R.id.threePointsPercentageTextView)
    TextView threePointsPercentageTextView;

//endregion

//region FreeThrows

    @BindView(R.id.freeThrowMakeLayout)
    ViewGroup freeThrowMakeLayout;

    @BindView(R.id.freeThrowMakeTextView)
    TextView freeThrowMakeTextView;

    @BindView(R.id.freeThrowMissLayout)
    ViewGroup freeThrowMissLayout;

    @BindView(R.id.freeThrowMissTextView)
    TextView freeThrowMissTextView;

    @BindView(R.id.freeThrowPtsTextView)
    TextView freeThrowPtsTextView;

    @BindView(R.id.freeThrowPercentageTextView)
    TextView freeThrowPercentageTextView;

//endregion

//region Bottom Row

    @BindView(R.id.assistLayout)
    ViewGroup assistLayout;

    @BindView(R.id.assistTextView)
    TextView assistTextView;

    @BindView(R.id.stealLayout)
    ViewGroup stealLayout;

    @BindView(R.id.stealTextView)
    TextView stealTextView;

    @BindView(R.id.blockLayout)
    ViewGroup blockLayout;

    @BindView(R.id.blockTextView)
    TextView blockTextView;

    @BindView(R.id.orebLayout)
    ViewGroup orebLayout;

    @BindView(R.id.orebTextView)
    TextView orebTextView;

    @BindView(R.id.drebLayout)
    ViewGroup drebLayout;

    @BindView(R.id.drebTextView)
    TextView drebTextView;

//endregion

//region Help

    @BindView(R.id.twoPointsHelpTextView)
    TextView twoPointsHelpTextView;

    @BindView(R.id.threePointsHelpTextView)
    TextView threePointsHelpTextView;

    @BindView(R.id.freeThrowHelpTextView)
    TextView freeThrowHelpTextView;

    @BindView(R.id.assistHelpTextView)
    TextView assistHelpTextView;

    @BindView(R.id.blockHelpTextView)
    TextView blockHelpTextView;

    @BindView(R.id.stealHelpTextView)
    TextView stealHelpTextView;

    @BindView(R.id.oredHelpTextView)
    TextView orebHelpTextView;

    @BindView(R.id.drepHelpTextView)
    TextView drebHelpTextView;

//endregion

    private WeakReference<GameActivity> weakParent;
    private ArrayList<StatisticGridButton> buttons;
    private Game game;

    //region StatisticGrid
    public StatisticGrid(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public StatisticGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatisticGrid(Context context) {
        super(context);
    }

//endregion

    public void updateUi() {
        GameStatistic statistic = game.getStatistic();

        twoPointsMakeTextView.setText(String.valueOf(statistic.getTwoPointsMake()));
        twoPointsMissTextView.setText(String.valueOf(statistic.getTwoPointsMiss()));
        twoPointsPtsTextView.setText(String.format(Locale.US, "%d", statistic.getTwoPointsMake() * 2));
        twoPointsPercentageTextView.setText(String.format(Locale.US, "%d", BasketballCalculator.calculatePercentage(statistic.getTwoPointsMake(), statistic.getTwoPointsMiss())));

        threePointsMakeTextView.setText(String.valueOf(statistic.getThreePointsMake()));
        threePointsMissTextView.setText(String.valueOf(statistic.getThreePointsMiss()));
        threePointsPtsTextView.setText(String.format(Locale.US, "%d", statistic.getThreePointsMake() * 3));
        threePointsPercentageTextView.setText(String.format(Locale.US, "%d", BasketballCalculator.calculatePercentage(statistic.getThreePointsMake(), statistic.getThreePointsMiss())));

        freeThrowMakeTextView.setText(String.valueOf(statistic.getFreeThrowMake()));
        freeThrowMissTextView.setText(String.valueOf(statistic.getFreeThrowMiss()));
        freeThrowPtsTextView.setText(String.format(Locale.US, "%d", statistic.getFreeThrowMake()));
        freeThrowPercentageTextView.setText(String.format(Locale.US, "%d", BasketballCalculator.calculatePercentage(statistic.getFreeThrowMake(), statistic.getFreeThrowMiss())));

        assistTextView.setText(String.valueOf(statistic.getAssist()));
        blockTextView.setText(String.valueOf(statistic.getBlock()));
        stealTextView.setText(String.valueOf(statistic.getSteal()));
        orebTextView.setText(String.valueOf(statistic.getOreb()));
        drebTextView.setText(String.valueOf(statistic.getDreb()));

        weakParent.get().updateUi();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
    }

    public void initWithGame(Game gameToTrack, GameActivity parent) {
        ButterKnife.bind(this);

        weakParent = new WeakReference<>(parent);
        this.game = gameToTrack;
        updateUi();

        buttons = new ArrayList<>();

        buttons.add(new StatisticGridButton(twoPointsMakeLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getTwoPointsMake();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setTwoPointsMake(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getTwoPointsMake();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setTwoPointsMake(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(twoPointsMissLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getTwoPointsMiss();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setTwoPointsMiss(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getTwoPointsMiss();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setTwoPointsMiss(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(threePointsMakeLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getThreePointsMake();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setThreePointsMake(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getThreePointsMake();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setThreePointsMake(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(threePointsMissLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getThreePointsMiss();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setThreePointsMiss(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getThreePointsMiss();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setThreePointsMiss(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(freeThrowMakeLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getFreeThrowMake();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setFreeThrowMake(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getFreeThrowMake();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setFreeThrowMake(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(freeThrowMissLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getFreeThrowMiss();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setFreeThrowMiss(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getFreeThrowMiss();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setFreeThrowMiss(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(assistLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getAssist();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setAssist(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getAssist();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setAssist(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(blockLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getBlock();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setBlock(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getBlock();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setBlock(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(stealLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getSteal();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setSteal(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getSteal();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setSteal(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(orebLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getOreb();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setOreb(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getOreb();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setOreb(currentValue);
                realm.commitTransaction();
            }
        }));

        buttons.add(new StatisticGridButton(drebLayout, this, new StatisticGridButton.ButtonLogic() {
            @Override
            public void onClick() {
                int currentValue = game.getStatistic().getDreb();
                currentValue += 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setDreb(currentValue);
                realm.commitTransaction();
            }

            @Override
            public void onLongClick() {
                int currentValue = game.getStatistic().getDreb();
                currentValue -= 1;

                if (isValueOutOfRange(currentValue)) {
                    return;
                }

                Realm realm = MainApplication.getInstance().getRealm();
                realm.beginTransaction();
                game.getStatistic().setDreb(currentValue);
                realm.commitTransaction();
            }
        }));


        twoPointsHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("A 2 Point Basket, or Field Goal:")
                        .content("A player scores 2 points when he/she makes the ball in the basket from anywhere inside the 3 point line, other than a Free Throw.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        threePointsHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("A 3 Point Basket, or Field Goal:")
                        .content("A player scores 3 points when he/she makes the ball in the basket from anywhere outside the 3 point line.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        freeThrowHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("Free Throw - Foul Shot")
                        .content("A player scores 1 point for each Free Throw shot made. A Free Throw is when a player is allowed to try to make a basket unopposed from the free throw line (or foul line) in front of the basket.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        assistHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("Assist:")
                        .content("A player gets an assist when he/she passes the ball to a teammate in a way that helps that person to score a basket. The pass is not an assist if it not directly help the player score the basket.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        stealHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("Steal:")
                        .content("A steal occurs when a player causes an opponent to lose the ball and it is retrieved by that player's team. This can be done by taking the ball away or deflecting the ball to a teammate. It is not a steal if the ball is already loose or if the opponent passes to the player.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        blockHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("Block - Blocked Shot:")
                        .content("A player blocks a shot when he legally deflects a shot attempted by an opponent causing him to miss the basket. If the referee calls a foul on the player, it is not counted as a blocked shot.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        orebHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("O-Reb - Offensive Rebound:")
                        .content(" ")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });

        drebHelpTextView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new MaterialDialog.Builder(weakParent.get())
                        .title("D-Reb - Defensive Rebound:")
                        .content("When a player catches the ball after an opponent has missed a shot at the basket.")
                        .positiveText("Ok")
                        .show();

                return true;
            }
        });


    }

    private boolean isValueOutOfRange(int value) {
        return value <= MainConstants.INSTANCE.getMinNumber() || value >= MainConstants.INSTANCE.getMaxNumber();
    }

}
