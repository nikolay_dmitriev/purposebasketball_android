package com.infinity.playwithpurposebasketball.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.NumberPicker;

import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.utility.GameTimer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimerPickerView extends LinearLayout {

    @BindView(R.id.minutesNumberPicker)
    public NumberPicker minutesNumberPicker;

    @BindView(R.id.secondsNumberPicker)
    public NumberPicker secondsNumberPicker;

    public TimerPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public TimerPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimerPickerView(Context context) {
        super(context);
    }

    public void initViews(int time) {
        ButterKnife.bind(this);
        minutesNumberPicker.setMinValue(0);
        minutesNumberPicker.setMaxValue(30);
        secondsNumberPicker.setMinValue(0);
        secondsNumberPicker.setMaxValue(59);

        int minutes = time / 60;
        int seconds = time - minutes * 60;
        minutesNumberPicker.setValue(minutes);
        secondsNumberPicker.setValue(seconds);
    }

    public int getTime() {
        //Limit to max value
        return Math.min(minutesNumberPicker.getValue() * 60 + secondsNumberPicker.getValue(), GameTimer.maxTime);
    }

}
