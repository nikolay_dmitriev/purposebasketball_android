package com.infinity.playwithpurposebasketball.network;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {
    private static IPlayWithPurposeAPI instance;

    public static IPlayWithPurposeAPI getInstance() {
        if (instance == null) {
            return updateInstance();
        } else {
            return instance;
        }
    }

    public static IPlayWithPurposeAPI updateInstance() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(loggingInterceptor);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.playwithpurposesports.org/")
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        instance = retrofit.create(IPlayWithPurposeAPI.class);
        return instance;
    }

    private NetworkManager() {

    }
}