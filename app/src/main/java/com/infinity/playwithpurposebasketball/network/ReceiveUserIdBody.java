package com.infinity.playwithpurposebasketball.network;

import com.google.gson.annotations.SerializedName;

public class ReceiveUserIdBody {

    @SerializedName("user_email")
    public String email;

    @SerializedName("user_pass")
    public String password;

    public static ReceiveUserIdBody create() {
        return new ReceiveUserIdBody();
    }

    public ReceiveUserIdBody setEmail(String email) {
        this.email = email;
        return this;
    }

    public ReceiveUserIdBody setPassword(String password) {
        this.password = password;
        return this;
    }

}
