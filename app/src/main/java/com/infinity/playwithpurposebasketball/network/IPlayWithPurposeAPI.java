package com.infinity.playwithpurposebasketball.network;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IPlayWithPurposeAPI {
    @POST("check_user.php")
    Call<Void> getUser(@Body ReceiveUserIdBody body);
}