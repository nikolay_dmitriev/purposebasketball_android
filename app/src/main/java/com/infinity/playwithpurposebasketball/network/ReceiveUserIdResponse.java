package com.infinity.playwithpurposebasketball.network;

import com.google.gson.annotations.SerializedName;

public class ReceiveUserIdResponse {
    @SerializedName("user_do")
    String userId;
}
