package com.infinity.playwithpurposebasketball.actions;

import android.content.Context;
import android.content.Intent;

import com.infinity.playwithpurposebasketball.database.dataObjects.Game;
import com.infinity.playwithpurposebasketball.utility.BasketballCalculator;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class ShareAction {

//region Variables

    private WeakReference<Context> weakContext;
    private final Game game;

//endregion

//region Public

    public ShareAction(Context context, Game game) {
        this.game = game;
        weakContext = new WeakReference<>(context);
    }

    public void run() {
        sendEmail();
    }

//endregion

//region Private

    private void sendEmail() {
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("text/plain");

        String result = buildTextToShare(game);
        sendEmailIntent.putExtra(Intent.EXTRA_TEXT, result);

        weakContext.get().startActivity(Intent.createChooser(sendEmailIntent, "Send Statistic"));
    }

//endregion

    private String buildTextToShare(Game game) {
        String result = "";
        result = String.format(Locale.US, "%s, #%s, %s\n", game.getName(), game.getPlayerNumber(), game.getTournamentName());

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        String gameDate = String.format("%s\n", dateFormat.format(game.getDate()));
        result += gameDate;
        result += String.format(Locale.US, "PWP Points: %d\n", BasketballCalculator.getPwpPoints(game));

        result += String.format(Locale.US, "Points: %d, Assists: %d, Steals: %d, Blocks: %d, Rebounds: %d",
                BasketballCalculator.getTotalPoints(game), game.getStatistic().getAssist(), game.getStatistic().getSteal(),
                game.getStatistic().getBlock(), game.getStatistic().getOreb() + game.getStatistic().getDreb());

        return result;
    }

}
