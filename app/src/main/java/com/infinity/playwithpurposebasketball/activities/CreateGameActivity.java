package com.infinity.playwithpurposebasketball.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.infinity.playwithpurposebasketball.MainApplication;
import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.database.dataObjects.Game;
import com.infinity.playwithpurposebasketball.database.dataObjects.GameStatistic;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class CreateGameActivity extends AppCompatActivity {

//region Static

    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 421;
    private static final int REQUEST_IMAGE = 432;
    public final static String gameIdKey = "gameId";

//endregion

//region Variables

    @BindView(R.id.layoutForAvatar)
    ViewGroup avatarImageViewLayout;

    @BindView(R.id.avatarImageView)
    ImageView avatarImageView;

    @BindView(R.id.enterGameNameEditText)
    EditText nameEditText;

    @BindView(R.id.enterTournamentEditText)
    EditText tournamentEditText;

    @BindView(R.id.enterPlayerNumberEditText)
    EditText playerNumberEditText;

    @BindView(R.id.changePhotoTextView)
    TextView changeTextView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String avatarUri;

    //If we are editing currentGame
    private Game game;

//endregion

//region Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        ButterKnife.bind(this);
        tryToGetGame();
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (game == null) {
            setTitle("Create Game");
        }

        if (game != null) {
            avatarUri = game.getAvatarPath();
            setTitle("Edit Game");
            Glide.with(this).load(game.getAvatarPath()).into(avatarImageView);
            nameEditText.setText(game.getName());
            tournamentEditText.setText(game.getTournamentName());
            playerNumberEditText.setText(game.getPlayerNumber());
        }

        avatarImageViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_create_game_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.done:
//                if (avatarUri == null || avatarUri.isEmpty()) {
//                    Toast.makeText(this, "Please set avatar", Toast.LENGTH_SHORT).show();
//                    return true;
//                }
                if (nameEditText.length() <= 0) {
                    Toast.makeText(this, "Please set game name", Toast.LENGTH_SHORT).show();
                    return true;
                }
                saveGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_READ_ACCESS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                List<String> path = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                String pathUri = path.get(0);
                pathUri = String.format("file://%s", pathUri);
                avatarUri = pathUri;

                avatarImageView.setBackground(null);

                Glide.with(this).load(pathUri).placeholder(R.drawable.placeholder).into(avatarImageView);
            }
        }
    }

//endregion

//region Private Photo Picker

    private void pickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.permission_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            MultiImageSelector selector = MultiImageSelector.create(CreateGameActivity.this);
            selector.showCamera(true);
            selector.count(1);
            selector.single();
            selector.start(CreateGameActivity.this, REQUEST_IMAGE);
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.permission_dialog_title)
                    .setMessage(rationale)
                    .setPositiveButton(R.string.permission_dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(CreateGameActivity.this, new String[]{permission}, requestCode);
                        }
                    })
                    .setNegativeButton(R.string.permission_dialog_cancel, null)
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

//endregion

//region Private

    private void saveGame() {
        final MaterialDialog progress = new MaterialDialog.Builder(CreateGameActivity.this)
                .title("Saving")
                .progress(true, 0)
                .cancelable(false)
                .show();

        Realm realm = MainApplication.getInstance().getRealm();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm bgRealm) {
                //create new game
                if (game == null) {
                    Game newGame = bgRealm.createObject(Game.class, Game.generateId());
                    GameStatistic statistic = bgRealm.createObject(GameStatistic.class);

                    newGame.setName(nameEditText.getText().toString());
                    newGame.setDate(new Date());
                    newGame.setStatistic(statistic);
                    newGame.setPlayerNumber(playerNumberEditText.getText().toString());
                    newGame.setTournamentName(tournamentEditText.getText().toString());
                    newGame.setGameLengthInSecond(0);
                    newGame.setAvatarPath(avatarUri);
                } else {
                    String userId = getIntent().getStringExtra(gameIdKey);
                    Game updatedGame = bgRealm.where(Game.class).equalTo("id", userId).findAll().first();
                    updatedGame.setName(nameEditText.getText().toString());
                    updatedGame.setAvatarPath(avatarUri);
                    updatedGame.setTournamentName(tournamentEditText.getText().toString());
                    updatedGame.setPlayerNumber(playerNumberEditText.getText().toString());
                }

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progress.dismiss();
                        finish();
                    }
                }, 1200);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                // Transaction failed and was automatically canceled.
                progress.dismiss();
                Toast.makeText(CreateGameActivity.this, "Error", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    private void tryToGetGame() {
        String userId = getIntent().getStringExtra(gameIdKey);
        if (userId == null) {
            return;
        }
        Realm realm = MainApplication.getInstance().getRealm();
        game = realm.where(Game.class).equalTo("id", userId).findAll().first();
    }

//endregion

}
