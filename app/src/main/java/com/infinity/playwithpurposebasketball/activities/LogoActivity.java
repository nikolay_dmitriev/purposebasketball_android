package com.infinity.playwithpurposebasketball.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.infinity.playwithpurposebasketball.R;

public class LogoActivity extends AppCompatActivity {


//region Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);
        finishActivityAfterDelay();
    }

    @Override
    public void onBackPressed() {
    }

//endregion

//region Private

    private void finishActivityAfterDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent tabBar = new Intent(LogoActivity.this, GamesListActivity.class);
                tabBar.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(tabBar);
            }
        }, 2700);
    }

//endregion

}
