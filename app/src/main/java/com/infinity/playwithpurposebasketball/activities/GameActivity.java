package com.infinity.playwithpurposebasketball.activities;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.infinity.playwithpurposebasketball.MainApplication;
import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.actions.ShareAction;
import com.infinity.playwithpurposebasketball.database.dataObjects.Game;
import com.infinity.playwithpurposebasketball.utility.BasketballCalculator;
import com.infinity.playwithpurposebasketball.utility.GameTimer;
import com.infinity.playwithpurposebasketball.views.StatisticGrid;
import com.infinity.playwithpurposebasketball.views.TimerPickerView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class GameActivity extends AppCompatActivity {

//region Static

    public final static String gameIdKey = "gameId";

//endregion

//region Variables

    @BindView(R.id.avatarImageView)
    ImageView avatarImageView;

    @BindView(R.id.fab)
    FloatingActionButton timerFabButton;

    @BindView(R.id.timerTextView)
    TextView timerTextView;

    @BindView(R.id.pointsTextView)
    TextView pointsTextView;

    @BindView(R.id.statisticGrid)
    StatisticGrid grid;

    @BindView(R.id.rewardLayout)
    LinearLayout rewardLayout;

    @BindView(R.id.rewardTextView)
    TextView rewardTextView;

    private GameTimer timer;

    private Game game;

//endregion

//region Activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        String userId = getIntent().getStringExtra(gameIdKey);
        Realm realm = MainApplication.getInstance().getRealm();
        game = realm.where(Game.class).equalTo("id", userId).findAll().first();

        Glide.with(this).load(game.getAvatarPath()).placeholder(R.drawable.place_basketball).into(avatarImageView);

        timerFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleTimer();
            }
        });

        String result = game.getName();
        String gameNumber = game.getPlayerNumber();

        if (!gameNumber.isEmpty()) {
            result += ", #" + gameNumber;
        }

        setTitle(result);

        timer = new GameTimer(game, timerTextView, this);
        grid.initWithGame(game, this);

        timerTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog dialog = new MaterialDialog.Builder(GameActivity.this)
                        .title("Set time")
                        .customView(R.layout.fragment_time_picker, false)
                        .positiveText("Ok")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                TimerPickerView view = (TimerPickerView) dialog.getCustomView();
                                timer.setTime(view.getTime());
                            }
                        }).show();
                TimerPickerView view = (TimerPickerView) dialog.getCustomView();
                view.initViews(timer.getTime());

                if(timer.isTimerActive()){
                    toggleTimer();
                }
            }
        });
    }

    private void toggleTimer() {
        if (timer.isTimerActive()) {
            int color = ContextCompat.getColor(this, R.color.colorAccent);
            timerFabButton.setBackgroundTintList(ColorStateList.valueOf(color));
            timerFabButton.setImageDrawable(ContextCompat.getDrawable(GameActivity.this, R.drawable.ic_timer_white_24dp));
            timer.stop();
        } else {
            int color = ContextCompat.getColor(this, R.color.colorAccentGreen);
            timerFabButton.setBackgroundTintList(ColorStateList.valueOf(color));
            timerFabButton.setImageDrawable(ContextCompat.getDrawable(GameActivity.this, R.drawable.ic_timer_off_white_24dp));
            timer.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer.isTimerActive()) {
            toggleTimer();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_game_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.share:
                new ShareAction(this, game).run();
                return true;
            case R.id.info:
                new MaterialDialog.Builder(this)
                        .title("Play with Purpose Basketball App:")
                        .content("Press the red circle timer button to start and stop the clock. Press the clock to manually edit the time on the clock.\n" +
                                "\n" +
                                "The circle buttons for all of the stats can be pressed to add one, press and hold the button to subtract one.\n" +
                                "\n" +
                                "The stats for the game can be shared by pressing the share button in upper right corner.\n" +
                                "\n" +
                                "Press and hold the button label name for a definition of each one.")
                        .positiveText("Ok")
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//endregion

//region Public

    public void updateUi() {
        updatePoints();
        updateReward();
    }

//endregion

//region Private

    private void updatePoints() {
        int points = BasketballCalculator.getTotalPoints(game);
        String formattedPoints = String.format(Locale.US, "Points: %d", points);
        pointsTextView.setText(formattedPoints);
    }

    private void updateReward() {
        String result = BasketballCalculator.getRewardName(game);
        if (result.isEmpty()) {
            rewardLayout.setVisibility(View.INVISIBLE);
            return;
        }
        rewardLayout.setVisibility(View.VISIBLE);
        rewardTextView.setText(result);
    }

//endregion

}
