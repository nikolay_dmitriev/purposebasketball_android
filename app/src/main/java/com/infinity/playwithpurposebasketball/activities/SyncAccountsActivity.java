package com.infinity.playwithpurposebasketball.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.infinity.playwithpurposebasketball.R;
import com.infinity.playwithpurposebasketball.network.NetworkManager;
import com.infinity.playwithpurposebasketball.network.ReceiveUserIdBody;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncAccountsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_accounts);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle("Connect Account");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_create_game_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.done:
                showFakeConnectAccount();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showFakeConnectAccount() {
        NetworkManager.getInstance().getUser(ReceiveUserIdBody.create().setEmail("dnv190@gmail.com").setPassword("12345")).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("g", "gg");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
//        final MaterialDialog progress = new MaterialDialog.Builder(this)
//                .progress(true, 0)
//                .title("Linking Account")
//                .cancelable(false)
//                .show();
//
//        toolbar.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                progress.setTitle("Done");
//                toolbar.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        progress.dismiss();
//                        finish();
//                    }
//                }, 3000);
//            }
//        }, 1000);

    }

}
