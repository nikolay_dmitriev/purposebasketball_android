package com.infinity.playwithpurposebasketball.database.dataObjects;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Game extends RealmObject {

//region Variables

    private String avatarPath;
    private String name;
    private Date date;
    private String playerNumber;
    private String tournamentName;
    private int gameLengthInSecond;

    @PrimaryKey
    private String id = "";

    private GameStatistic statistic;

//endregion

//region Public

    public Game(String name, String avatarPath, GameStatistic statistic) {
        setName(name);
        setAvatarPath(avatarPath);
        setStatistic(statistic);
    }

    public Game() {

    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public static String generateId() {
        return UUID.randomUUID().toString();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public GameStatistic getStatistic() {
        return statistic;
    }

    public void setStatistic(GameStatistic statistic) {
        this.statistic = statistic;
    }

    public int getGameLengthInSecond() {
        return gameLengthInSecond;
    }

    public void setGameLengthInSecond(int gameLengthInSecond) {
        this.gameLengthInSecond = gameLengthInSecond;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }


//endregion

}
