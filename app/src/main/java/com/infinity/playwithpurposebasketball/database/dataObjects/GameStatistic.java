package com.infinity.playwithpurposebasketball.database.dataObjects;

import io.realm.RealmObject;

public class GameStatistic extends RealmObject {

//region Private

    private int twoPointsMake;
    private int twoPointsMiss;

    private int threePointsMake;
    private int threePointsMiss;

    private int freeThrowMake;
    private int freeThrowMiss;

    private int assist;
    private int block;
    private int steal;
    private int oreb;
    private int dreb;

    public GameStatistic() {
        twoPointsMake = 0;
        twoPointsMiss = 0;
    }

    public int getTwoPointsMake() {
        return twoPointsMake;
    }

    public void setTwoPointsMake(int twoPointsMake) {
        this.twoPointsMake = twoPointsMake;
    }

    public int getTwoPointsMiss() {
        return twoPointsMiss;
    }

    public void setTwoPointsMiss(int twoPointsMiss) {
        this.twoPointsMiss = twoPointsMiss;
    }

    public int getThreePointsMake() {
        return threePointsMake;
    }

    public void setThreePointsMake(int threePointsMake) {
        this.threePointsMake = threePointsMake;
    }

    public int getThreePointsMiss() {
        return threePointsMiss;
    }

    public void setThreePointsMiss(int threePointsMiss) {
        this.threePointsMiss = threePointsMiss;
    }

    public int getFreeThrowMake() {
        return freeThrowMake;
    }

    public void setFreeThrowMake(int freeThrowMake) {
        this.freeThrowMake = freeThrowMake;
    }

    public int getFreeThrowMiss() {
        return freeThrowMiss;
    }

    public void setFreeThrowMiss(int freeThrowMiss) {
        this.freeThrowMiss = freeThrowMiss;
    }

    public int getAssist() {
        return assist;
    }

    public void setAssist(int assist) {
        this.assist = assist;
    }

    public int getBlock() {
        return block;
    }

    public void setBlock(int block) {
        this.block = block;
    }

    public int getSteal() {
        return steal;
    }

    public void setSteal(int steal) {
        this.steal = steal;
    }

    public int getOreb() {
        return oreb;
    }

    public void setOreb(int oreb) {
        this.oreb = oreb;
    }

    public int getDreb() {
        return dreb;
    }

    public void setDreb(int dreb) {
        this.dreb = dreb;
    }

//endregion
}
