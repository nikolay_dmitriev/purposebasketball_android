package com.infinity.playwithpurposebasketball;

import android.app.Application;
import android.content.Intent;

import com.afollestad.materialdialogs.MaterialDialog;
import com.infinity.playwithpurposebasketball.activities.LogoActivity;

import io.realm.Realm;

public class MainApplication extends Application {

    private static MainApplication instance;
    private Realm realm;

    public static MainApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Realm.init(this);
        // Open the Realm for the UI thread.
        realm = Realm.getDefaultInstance();

//        Intent tabBar = new Intent(this, LogoActivity.class);
//        tabBar.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(tabBar);
    }

    public Realm getRealm() {
        return realm;
    }
}
